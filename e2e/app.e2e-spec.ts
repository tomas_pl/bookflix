import { BookflixPage } from './app.po';

describe('bookflix App', function() {
  let page: BookflixPage;

  beforeEach(() => {
    page = new BookflixPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
