import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { BookService } from '../shared/model/book.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Libro } from '../shared/model/libro';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UploadService } from '../shared/model/upload.service';
import { Upload } from '../shared/model/upload';

@Component({
    selector: 'app-list-book-admin',
    templateUrl: './list-book-admin.component.html',
    styleUrls: ['./list-book-admin.component.css']
})
export class ListBookAdminComponent implements OnInit {
    libros$: Observable<Libro[]>;
    libros: any;
    libro: Libro;
    form: FormGroup;
    currentUpload: Upload;
    selectedFiles: FileList;

    constructor(private bookService: BookService, private fb: FormBuilder, private upSvc: UploadService) {

    }


    ngOnInit() {
        this.libros$ = this.bookService.findAllLibro();
        this.libros$.subscribe(response => {
            this.libros = response.sort((a, b) => a['$key'] > b['$key'] ? 1 : a['$key'] === b['$key'] ? 0 : -1).reverse();
        })

    }

    saveBookInfo(libro: Libro, campo: string, value: any) {
        this.bookService.saveBook(libro, campo, value);
    }

    createNewBook() {
        this.bookService.createNewBook();
    }

    deleteBook(book: Libro) {
        this.bookService.deleteBook(book.$key);
    }

    uploadSingle(libro: Libro, campo: string) {
        let file = this.selectedFiles.item(0);
        this.currentUpload = new Upload(file);
        this.upSvc.pushUpload(this.currentUpload, libro, campo);

    }

    detectFiles(event, libro: Libro, campo: string) {
        this.selectedFiles = event.target.files;
        this.uploadSingle(libro, campo);
    }

}
