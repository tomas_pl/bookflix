import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBookAdminComponent } from './list-book-admin.component';

describe('ListBookAdminComponent', () => {
  let component: ListBookAdminComponent;
  let fixture: ComponentFixture<ListBookAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBookAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBookAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
