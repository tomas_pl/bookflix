import { Component, OnInit, OnDestroy } from '@angular/core';
import {Observable} from "rxjs";
import {BookService} from "../shared/model/book.service";
import {Libro} from "../shared/model/libro";
import {Router, ActivatedRoute} from "@angular/router";
import {AppComponent} from "../app.component";
import {AlumnoService} from "../shared/model/alumno.service";
import {Alumno} from "../shared/model/alumno";

@Component({
  selector: 'app-list-book',
  templateUrl: './list-book.component.html',
  styleUrls: ['./list-book.component.css']
})
export class ListBookComponent implements OnInit, OnDestroy{
  public portada1 = 'https://firebasestorage.googleapis.com/v0/b/bookflix-4230d.appspot.com/o/portada.png?alt=media&token=e752e171-a069-4816-a557-2c043b5acfdb'
  // public portada1 = 'https://firebasestorage.googleapis.com/v0/b/bookflix-4230d.appspot.com/o/portada1.png?alt=media&token=bc2bd801-7d01-4ae8-8e18-5d2702f0a5e0'
  public portada2 = 'https://firebasestorage.googleapis.com/v0/b/bookflix-4230d.appspot.com/o/portada2.png?alt=media&token=3c762682-07ba-4dc5-9b4c-f964966801a2'
  public portada3 = 'https://firebasestorage.googleapis.com/v0/b/bookflix-4230d.appspot.com/o/portada3.png?alt=media&token=de4e7851-89cc-4931-bf07-37ce99ece20c'
  public p1 : boolean;
  public p2 : boolean;
  public p3 : boolean;
  public keyword: string;
  public activeSearchLibro: boolean;
  private subscFind = null;
  private subscActiveSearch = null;
  libro$: Observable<Libro[]>;
  listLibro;
  constructor( private libroService: BookService, private router: Router, private route:ActivatedRoute,private alumnoService:AlumnoService) {
  }




  ngOnInit() {
    const num = 1;
    this.activeSearchLibro = false;
    this.p1 = num===1;
    // this.p2 = num===2;
    // this.p3 = num===3;

    this.subscFind = this.libroService.findAllLibro().subscribe(response => {
      this.listLibro = response
    });
    this.subscActiveSearch = this.libroService.findActiveSearchLibro().subscribe(response => {
      this.activeSearchLibro = response!==''
      this.keyword = response
      this.p1 = num===1 && !this.activeSearchLibro
      // this.p2 = num===2 && !this.activeSearchLibro
      // this.p3 = num===3 && !this.activeSearchLibro
    });

  } 

  private aleatorio(a,b) {
    return Math.round(Math.random()*(b-a)+parseInt(a));
    }
  ngOnDestroy(){
    this.subscActiveSearch.unsubscribe();
    this.subscFind.unsubscribe();
    }
}
