export class Leido {
  constructor(
    public libro:string,
    public devolvio:boolean,
    public foto:string
  ) {

  }

  static fromJsonList(array): Leido[] {
    return array.map(Leido.fromJson);
  }

  static fromJson({libro, devolvio, foto}):Leido {
    return new Leido(
      libro,
      devolvio,
      foto
    );
  }


}












