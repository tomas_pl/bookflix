export class Prestado {
  constructor(
    public alumno:string,
    public nya:string,
    public libro:string,
    public titulo:string,
    public foto:string,
    public fechaPrestamo:number,
    public fechaDevolucion:number,
    public key:string
  ) {

  }

  static fromJsonList(array): Prestado[] {
    return array.map(Prestado.fromJson);
  }

  static fromJson({alumno, nya, libro,titulo, foto, fechaPrestamo, fechaDevolucion, $key}):Prestado {
    return new Prestado(
      alumno,nya, libro, titulo, foto, fechaPrestamo, fechaDevolucion, $key
    );
  }


}












