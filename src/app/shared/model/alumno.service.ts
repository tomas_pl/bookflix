import { Injectable, Inject } from "@angular/core";
import { Observable, Subject } from "rxjs/Rx";
import { Http } from "@angular/http";
import { Alumno } from "./alumno";
import "firebase/database";
import { AngularFireDatabase } from "angularfire2/database/database";
import { FirebaseRef, AngularFire, AuthProviders, AuthMethods } from "angularfire2";
import {
  ActivatedRoute,
  Router,
  ActivatedRouteSnapshot
} from "@angular/router";

@Injectable()
export class AlumnoService {
  sdkDb: any;
  dbSearch: any;
  librosdb: any;
  alumnoId: string;

  constructor(
    private db: AngularFireDatabase,
    @Inject(FirebaseRef) fb,
    private http: Http,
    private router: Router,
    private route: ActivatedRoute,
    private af: AngularFire
  ) {
    this.sdkDb = fb.database().ref();
    this.dbSearch = fb.database().ref("/");
    this.librosdb = fb.database().ref("/libros");
  }

  findAllAlumno(): Observable<Alumno[]> {
    return this.db
      .list("alumnos")
      .map(arr => arr.reverse())
      .map(Alumno.fromJsonList);
  }

  firebaseUpdate(dataToSave) {
    const subject = new Subject();
    this.sdkDb.update(dataToSave).then(
      val => {
        subject.next(val);
        subject.complete();
      },
      err => {
        subject.error(err);
        subject.complete();
      }
    );
    return subject.asObservable();
  }

  findAlumnoById(id: string): Observable<Alumno> {
    return this.db
      .object("alumnos/" + id)

      .map(results => {
        console.log("Alumno.fromJson(results)", Alumno.fromJson(results));
        return Alumno.fromJson(results);
      });
  }

  findAlumnoByIdSinParam(): Observable<Alumno> {
    const alum = "1";
    return this.db
      .list("alumnos", {
        query: {
          orderByChild: "id",
          equalTo: alum
        }
      })
      .filter(results => results && results.length > 0)
      .map(results => Alumno.fromJson(results[0]));
  }

  dameNombre(): Observable<Alumno> {
    this.alumnoId = this.route.snapshot.params["id"];
    return this.db
      .list("alumnos", {
        query: {
          orderByChild: "id",
          equalTo: this.alumnoId
        }
      })
      .filter(results => results && results.length > 0)
      .map(results => Alumno.fromJson(results[0]));
  }

  saveAlumno(alumno: Alumno, campo: string, value: any): Observable<any> {
    let dataToSave = {};
    dataToSave["alumnos/" + alumno.$key + "/" + campo] = value;
    return this.firebaseUpdate(dataToSave);
  }

  createNewAlumno(): Observable<any> {
    const random = Math.floor(Math.random() * (29 - 1)) + 1;
    const random2 = random < 10 ? "0" + random : random.toString();
    const alumnoToSave = Object.assign(
      { nombre: "" },
      { apellido: "" },
      { img: random2 },
      { id: "" }
    );
    const newAlumnoKey = this.sdkDb.child("alumnos/").push().key;

    let envioAlumno = {};

    envioAlumno["alumnos/" + newAlumnoKey] = alumnoToSave;
    return this.firebaseUpdate(envioAlumno);
  }
  deleteAlumno(alummnokey: string) {
    this.sdkDb.child("alumnos/" + alummnokey).remove();
  }

  retirar(idLibro: string, idAlumno: string) {
    let dataToSave = {};
    dataToSave["alumnos/" + idAlumno + "/libro"] = idLibro;
    return this.firebaseUpdate(dataToSave);
  }

  resetAll() {
    const _this = this;
    this.dbSearch.once("value").then(function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        if (childSnapshot.key !== "libros" && childSnapshot.key !== "uploads") {
          _this.sdkDb.child(childSnapshot.key).remove();
        }
      });
    });
    this.librosdb.once("value").then(function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        _this.sdkDb.child("libros/" + childSnapshot.key + "/promedio").remove();
        _this.sdkDb
          .child("libros/" + childSnapshot.key + "/promedioX")
          .remove();
        _this.sdkDb.child("libros/" + childSnapshot.key + "/loTiene").remove();
      });
    });
  }

  loginWithGoogle(user, password) {
    var creds: any = {email: user+"@bookflix.com", password};
    return this.af.auth.login(creds).then(result => { console.log('result', result)});

  }
}
