import { Injectable, Inject } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFire, AngularFireDatabase, FirebaseListObservable } from 'angularfire2';
import { Upload } from './upload';
import { BookService } from './book.service';
import { Libro } from './libro';

@Injectable()
export class UploadService {

    constructor(private af: AngularFire, private db: AngularFireDatabase, private bookService: BookService) { }

    private basePath: string = '/uploads';
    uploads: FirebaseListObservable<Upload[]>;

    pushUpload(upload: Upload, libro: Libro, campo: string) {
        let storageRef = firebase.storage().ref();
        let uploadTask = storageRef.child(`${this.basePath}/${upload.file.name}`).put(upload.file);

        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
                // upload in progress
                upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            },
            (error) => {
                // upload failed
            },
            () => {
                // upload success
                upload.url = uploadTask.snapshot.downloadURL
                upload.name = upload.file.name
                this.saveFileData(upload, libro, campo)
            }
        );
    }



    // Writes the file details to the realtime db
    private saveFileData(upload: Upload, libro: Libro, campo: string) {
        this.bookService.saveBook(libro, campo, upload.url);
        this.db.list(`${this.basePath}/`).push(upload);
    }
}
