


export class Alumno {


  constructor(
    public id: string,
    public $key: string,
    public nombre: string,
    public apellido: string,
    public img: string,
    public imgLibro: string,
    public libro: string,
  ) {

  }

  static fromJsonList(array): Alumno[] {
    return array.map(Alumno.fromJson);
  }

  static fromJson({ id, $key, nombre, apellido, img, imgLibro, libro }): Alumno {
    return new Alumno(
      id,
      $key,
      nombre,
      apellido,
      img,
      imgLibro,
      libro
    );
  }


}












