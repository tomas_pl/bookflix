import { Injectable, Inject } from "@angular/core";
import { Observable, Subject } from "rxjs/Rx";

import { Http } from "@angular/http";
import { Libro } from "./libro";
import "firebase/database";
import { AngularFireDatabase } from "angularfire2/database/database";
import { FirebaseRef } from "angularfire2";
import { Calificacion } from "./calificacion";
import { Comentario } from "./comentario";
import { Leido } from "./leido";
import { Alumno } from "./alumno";
import { Prestado } from "./prestado";

@Injectable()
export class BookService {
  sdkDb: any;
  valor: number;
  total: number;
  promedio: number;
  private internalList;
  private listLibro = new Subject<Libro[]>();
  public listLibro$ = this.listLibro.asObservable();

  private searchLibro = new Subject<string>();
  public searchLibro$ = this.searchLibro.asObservable();

  private ficheroLibro = new Subject<Libro[]>();
  public ficheroLibro$ = this.ficheroLibro.asObservable();

  private estadoActual = new Subject<Prestado[]>();
  public estadoActual$ = this.estadoActual.asObservable();

  constructor(
    private db: AngularFireDatabase,
    @Inject(FirebaseRef) fb,
    private http: Http
  ) {
    this.sdkDb = fb.database().ref();
  }

  findAllLibro(): Observable<Libro[]> {
    this.db
      .list("libros")
      .map(arr => arr.reverse())
      .map(Libro.fromJsonList)
      .subscribe(response => {
        this.internalList = this.shuffle(response);
        this.listLibro.next(response);
      });

    return this.listLibro$;
  }

  searchBook(keyword) {
    const word = keyword.toLowerCase();
    const searcher = this.internalList.filter(
      item =>
        item.autor.toLowerCase().indexOf(word) > -1 ||
        item.editorial.toLowerCase().indexOf(word) > -1 ||
        item.titulo.toLowerCase().indexOf(word) > -1
    );
    this.listLibro.next(searcher);
    this.searchLibro.next(keyword);
  }

  findActiveSearchLibro() {
    return this.searchLibro$;
  }

  findLeidos(alumno: Alumno): Observable<Leido[]> {
    return this.db.list(alumno.$key).map(Leido.fromJsonList);
  }

  firebaseUpdate(dataToSave) {
    const subject = new Subject();
    this.sdkDb.update(dataToSave).then(
      val => {
        subject.next(val);
        subject.complete();
      },
      err => {
        subject.error(err);
        subject.complete();
      }
    );
    return subject.asObservable();
  }

  findBookByUrl(url: string): Observable<Libro> {
    return this.db
      .list("libros", {
        query: {
          orderByChild: "url",
          equalTo: url
        }
      })
      .filter(results => results && results.length > 0)
      .map(results => Libro.fromJson(results[0]));
  }

  findBookByCalif(booKey: string, alumnoId: string): Observable<Calificacion> {
    return this.db.object(booKey + "/" + alumnoId);
  }

  retiroDeLibro(
    alumnoId: string,
    libroId: string,
    libroTitle: string,
    libroFoto: string
  ): Observable<any> {
    const lessonToSave = Object.assign(
      { devolvio: false },
      { libro: libroTitle },
      { foto: libroFoto }
    );
    let dataToSave = {};
    dataToSave[alumnoId + "/" + libroId] = lessonToSave;
    return this.firebaseUpdate(dataToSave);
  }

  savePromedioLibro(
    alumnoId: string,
    calif: string,
    libroId: string
  ): Observable<any> {
    this.valor = 0;
    this.total = 0;
    this.promedio = 0;
    const lessonToSave = Object.assign({}, calif);
    let dataToSave = {};
    dataToSave[libroId + "/" + alumnoId] = calif;
    return this.firebaseUpdate(dataToSave);
  }

  actualizoPromedios(alumnoId, calif, libroId) {
    var valor = 0;
    this.db.list(libroId).forEach(element => {
      this.promedio = 0;
      for (let i = 0; i < element.length; i++) {
        setTimeout(() => {
          var total = element.length;
          valor += element[i].$value;
          this.promedio = valor / total;
          var tempo = this.promedio;
          if (i + 1 == element.length) {
            let dataToSave = {};
            dataToSave["libros/" + libroId + "/promedio"] = tempo;
            dataToSave["libros/" + libroId + "/promedioX"] = tempo * 20;
            this.llamadoDoble(dataToSave);
            return this.firebaseUpdate(dataToSave);
          }
        }, 700);
      }
    });
  }
  llamadoDoble(dataToSave) {
    return this.firebaseUpdate(dataToSave);
  }

  createNewComment(
    comment: any,
    libroKey: string,
    time: number,
    titulo: string
  ): Observable<any> {
    const commentToSave = Object.assign({}, comment);
    commentToSave["time"] = time;
    commentToSave["libroKey"] = libroKey;
    commentToSave["titulo"] = titulo;
    const newCommentKey = this.sdkDb
      .child("comentariosPorLibro/" + libroKey + "/")
      .push().key;
    commentToSave["commentKey"] = newCommentKey;

    let envioComment = {};
    let envioComment2 = {};

    envioComment[
      "comentariosPorLibro/" + libroKey + "/" + newCommentKey
    ] = commentToSave;
    envioComment2["comentarios/" + newCommentKey] = commentToSave;
    this.firebaseUpdate(envioComment2);
    return this.firebaseUpdate(envioComment);
  }
  createNewBook(): Observable<any> {
    const bookToSave = Object.assign(
      { autor: "" },
      { descripcion: "" },
      { editorial: "" },
      { titulo: "" },
      { ilustrador: "" },
      { linkYoutube: "" },
      {
        foto:
          "https://firebasestorage.googleapis.com/v0/b/bookflix-4230d.appspot.com/o/book.jpg?alt=media&token=e9772088-5b4b-4125-ba4b-e102523bf6e6"
      }
    );
    const newBookKey = this.sdkDb.child("libros/").push().key;
    bookToSave["url"] = newBookKey;
    let envioBook = {};

    envioBook["libros/" + newBookKey] = bookToSave;
    return this.firebaseUpdate(envioBook);
  }

  saveBook(libro: Libro, campo: string, value: any): Observable<any> {
    let dataToSave = {};
    dataToSave["libros/" + libro.$key + "/" + campo] = value;
    return this.firebaseUpdate(dataToSave);
  }

  imprimirComentariosLibro(libro: string): Observable<Comentario[]> {
    return this.db
      .list("comentariosPorLibro/" + libro + "/")
      .map(Comentario.fromJsonList);
  }

  findComentariosAdmin(): Observable<Comentario[]> {
    return this.db.list("comentarios/").map(Comentario.fromJsonList);
  }

  deleteBook(librokey: string) {
    this.sdkDb.child("libros/" + librokey).remove();
  }

  deleteComment(commentkey: string, librokey: string) {
    console.log("librokey", librokey);
    console.log("commentkey", commentkey);
    this.sdkDb
      .child("comentariosPorLibro/" + librokey + "/" + commentkey)
      .remove();
    this.sdkDb.child("comentarios/" + commentkey).remove();
  }
  private shuffle(array) {
    var currentIndex = array.length,
      temporaryValue,
      randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  retirar(idLibro: string, idAlumno: string) {
    let dataToSave = {};
    dataToSave["libros/" + idLibro + "/loTiene"] = idAlumno;
    return this.firebaseUpdate(dataToSave);
  }

  agregarEnFichero(libroKey: string, idAlumno: string) {
    const date = new Date();
    const fichaToSave = { idAlumno, timestamp: date.getTime() };
    const newFichaKey = this.sdkDb.child("fichero/" + libroKey + "/").push()
      .key;

    let envioFicha = {};

    envioFicha["fichero/" + libroKey + "/list/" + newFichaKey] = fichaToSave;

    this.firebaseUpdate(envioFicha);
  }

  getFicheroLibros() {
    this.db.list("fichero").subscribe(response => {
      this.ficheroLibro.next(response);
    });
    return this.ficheroLibro$;
  }
  agregarAEstadoActual(
    alumno,
    nombreAlumno,
    apellidoAlumno,
    libro,
    titulo,
    foto
  ) {
    this.retiroDeLibro(alumno, libro, titulo, foto);

    const date = new Date();
    const newEstado = {
      alumno,
      nya: nombreAlumno + " " + apellidoAlumno,
      libro,
      titulo,
      foto,
      fechaPrestamo: date.getTime(),
      fechaDevolucion: this.addDaysToTime(7, date.getTime())
    };
    const newFichaKey = this.sdkDb.child("estado/").push().key;
    newEstado["key"] = newFichaKey;
    let envioFicha = {};

    envioFicha["estado/" + newFichaKey] = newEstado;

    this.firebaseUpdate(envioFicha);
  }

  private addDaysToTime(days: number, timestamp: number) {
    return 60000 * 60 * 24 * days + timestamp;
  }

  devolucionEstado(idLibro, idAlumno, estadoKey) {
    let dataToSave = {};
    dataToSave[idAlumno + "/" + idLibro + "/devolvio"] = true;
    this.sdkDb.child("estado/" + estadoKey).remove();
    this.sdkDb.child("libros/" + idLibro + "/loTiene").remove();
    this.sdkDb.child("alumnos/" + idAlumno + "/libro").remove();
    return this.firebaseUpdate(dataToSave);
  }

  findPrestados() {
    this.db.list("estado").subscribe(response => {
      this.estadoActual.next(response);
    });
    return this.estadoActual$;
  }
}
