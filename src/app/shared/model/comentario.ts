export class Comentario {
  constructor(
    public alumno: string,
    public comentario: string,
    public foto: string,
    public time: number,
    public titulo: string,
    public commentKey: string,
    public libroKey: string
  ) {

  }

  static fromJsonList(array): Comentario[] {
    return array.map(Comentario.fromJson);
  }

  static fromJson({alumno, comentario, foto, time, titulo,commentKey, libroKey}): Comentario {
    return new Comentario(
      alumno,
      comentario,
      foto,
      time, titulo,
      commentKey, libroKey
    );
  }


}












