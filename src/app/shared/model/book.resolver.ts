import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Rx";
import {Injectable} from "@angular/core";
import {Libro} from "./libro";
import {BookService} from "./book.service";


@Injectable()
export class BookResolver implements Resolve<Libro> {


  constructor(private bookService: BookService) {

  }

  resolve(route:ActivatedRouteSnapshot,
          state:RouterStateSnapshot):Observable<Libro> {

    return this.bookService
      .findBookByUrl(route.params['book'])
      .first();
  }

}
