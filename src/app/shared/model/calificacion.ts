export class Calificacion {
  constructor(
    public $key:string,
    public $value:string
  ) {

  }

  static fromJsonList(array): Calificacion[] {
    return array.map(Calificacion.fromJson);
  }

  static fromJson({$key, $value}):Calificacion {
    return new Calificacion(
      $key,
      $value
    );
  }


}












