


import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Alumno} from "./alumno";
import {Observable} from "rxjs/Rx";
import {Injectable} from "@angular/core";
import {AlumnoService} from "./alumno.service";


@Injectable()
export class AlumnoResolver implements Resolve<Alumno> {


  constructor(private alumnoService: AlumnoService) {

  }

  resolve(route:ActivatedRouteSnapshot,
          state:RouterStateSnapshot):Observable<Alumno> {

    return this.alumnoService
      .findAlumnoById(route.params['id'])
      .first();
  }

}
