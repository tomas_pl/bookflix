


export class Libro {


  constructor(
    public $key: string,
    public autor: string,
    public descripcion: string,
    public editorial: string,
    public foto: string,
    public titulo: string,
    public url: string,
    public promedio: number,
    public promedioX: number,
    public loTiene: string,
    public ilustrador: string,
    public linkYoutube: string
  ) {

  }

  static fromJsonList(array): Libro[] {
    return array.map(Libro.fromJson);
  }

  static fromJson({ $key, autor, descripcion, editorial, foto, titulo, url, promedio, promedioX, loTiene, ilustrador, linkYoutube}): Libro {
    return new Libro(
      $key,
      autor,
      descripcion,
      editorial,
      foto,
      titulo,
      url,
      promedio,
      promedioX,
      loTiene,
      ilustrador,
      linkYoutube
    );
  }


}












