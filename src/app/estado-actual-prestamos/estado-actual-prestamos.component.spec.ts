import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoActualPrestamosComponent } from './estado-actual-prestamos.component';

describe('EstadoActualPrestamosComponent', () => {
  let component: EstadoActualPrestamosComponent;
  let fixture: ComponentFixture<EstadoActualPrestamosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoActualPrestamosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoActualPrestamosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
