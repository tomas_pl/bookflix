
import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {BookService} from "../shared/model/book.service";
import {Leido} from "../shared/model/leido";
import {Alumno} from "../shared/model/alumno";
import {ActivatedRoute, Router} from "@angular/router";
import { Prestado } from 'app/shared/model/prestado';

@Component({
  selector: 'app-estado-actual-prestamos',
  templateUrl: './estado-actual-prestamos.component.html',
  styleUrls: ['./estado-actual-prestamos.component.css']
})
export class EstadoActualPrestamosComponent implements OnInit {

  prestados$: Observable<Prestado[]>;
  alumno:Alumno;
  titulo;
  nya;
  idLibro;
  idAlumno;
  key;


  constructor( private bookService: BookService,private route:ActivatedRoute,private router:Router) {
    route.data
      .subscribe(
        data => {this.alumno = data['alumno']}
      );
  }

  ngOnInit() {
    this.prestados$ = this.bookService.findPrestados();
  }

  devolvio(idLibro, idAlumno, key){
    this.bookService.devolucionEstado(idLibro, idAlumno, key)
  }

  devolvioModal(idLibro, idAlumno, key, titulo, nya){
    this.titulo = titulo;
    this.nya = nya;
    this.idLibro = idLibro;
    this.idAlumno = idAlumno;
    this.key = key;
  }

}
