import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { ListAlumnosComponent } from './list-alumnos/list-alumnos.component';
import { ListBookComponent } from './list-book/list-book.component';
import { BookComponent } from './book/book.component';
import { ListComentariosComponent } from './list-comentarios/list-comentarios.component';
import {RouterModule} from "@angular/router";
import {routerConfig} from "./router.config";
import {AlumnoService} from "./shared/model/alumno.service";
import {BookService} from "./shared/model/book.service";
import {AngularFireModule, AuthProviders, AuthMethods} from "angularfire2";

import {firebaseConfig} from "../environments/firebase.config";
import {AlumnoResolver} from "./shared/model/alumno.resolver";
import {BookResolver} from "./shared/model/book.resolver";
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { ListLeidosComponent } from './list-leidos/list-leidos.component';
import { ListBookAdminComponent } from './list-book-admin/list-book-admin.component';
import {UploadService} from './shared/model/upload.service';
import { ListAlumnosAdminComponent } from './list-alumnos-admin/list-alumnos-admin.component';
import { FicheroLibroComponent } from './fichero-libro/fichero-libro.component';
import { LOCALE_ID } from '@angular/core';
import { EstadoActualPrestamosComponent } from './estado-actual-prestamos/estado-actual-prestamos.component';
import { ListComentariosAdminComponent } from './list-comentarios-admin/list-comentarios-admin.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ListAlumnosComponent,
    ListBookComponent,
    BookComponent,
    ListComentariosComponent,
    ListLeidosComponent,
    ListBookAdminComponent,
    ListAlumnosAdminComponent,
    FicheroLibroComponent,
    EstadoActualPrestamosComponent,
    ListComentariosAdminComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    ModalModule.forRoot(),
    BootstrapModalModule,
    FormsModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routerConfig)

  ],
  providers: [
    AlumnoService,
    BookService,
    AlumnoResolver,
    BookResolver,
    UploadService,
    { provide: LOCALE_ID, useValue: "es" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
