import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { Libro } from "../shared/model/libro";
import { FormBuilder, FormGroup } from "@angular/forms";
import { UploadService } from "../shared/model/upload.service";
import { Upload } from "../shared/model/upload";
import { BookService } from "../shared/model/book.service";
import { Alumno } from "../shared/model/alumno";
import { AlumnoService } from "../shared/model/alumno.service";

@Component({
  selector: "app-list-alumnos-admin",
  templateUrl: "./list-alumnos-admin.component.html",
  styleUrls: ["./list-alumnos-admin.component.css"]
})
export class ListAlumnosAdminComponent implements OnInit {
  alumnos$: Observable<Alumno[]>;
  alumno: Alumno;
  form: FormGroup;

  constructor(private alumnoService: AlumnoService, private fb: FormBuilder) {
    /*this.form = this.fb.group({
            titulo: [this.libro.titulo],
            descripcion: [this.libro.descripcion ],
            autor: [this.libro.autor ],
            editorial: [this.libro.editorial ]
        });*/
  }

  ngOnInit() {
    this.alumnos$ = this.alumnoService.findAllAlumno();
  }

  saveAlumnoInfo(alumno: Alumno, campo: string, value: any) {
    this.alumnoService.saveAlumno(alumno, campo, value);
  }
  resetAll() {
    this.alumnoService.resetAll();
  }
  createNewAlumno() {
    this.alumnoService.createNewAlumno();
  }

  deleteAlumno(alumno: Alumno) {
    this.alumnoService.deleteAlumno(alumno.$key);
  }
}
