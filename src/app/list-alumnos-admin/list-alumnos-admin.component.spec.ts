import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAlumnosAdminComponent } from './list-alumnos-admin.component';

describe('ListAlumnosAdminComponent', () => {
  let component: ListAlumnosAdminComponent;
  let fixture: ComponentFixture<ListAlumnosAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAlumnosAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAlumnosAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
