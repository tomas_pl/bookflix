

import {Route} from "@angular/router";
import {ListAlumnosComponent} from "./list-alumnos/list-alumnos.component";
import {ListBookComponent} from "./list-book/list-book.component";
import {MenuComponent} from "./menu/menu.component";
import {AlumnoResolver} from "./shared/model/alumno.resolver";
import {BookComponent} from "./book/book.component";
import {BookResolver} from "./shared/model/book.resolver";
import {ListLeidosComponent} from "./list-leidos/list-leidos.component";
import {ListBookAdminComponent} from './list-book-admin/list-book-admin.component';
import {ListAlumnosAdminComponent} from './list-alumnos-admin/list-alumnos-admin.component';
import { FicheroLibroComponent } from "./fichero-libro/fichero-libro.component";
import { EstadoActualPrestamosComponent } from "./estado-actual-prestamos/estado-actual-prestamos.component";
import { ListComentariosAdminComponent } from "./list-comentarios-admin/list-comentarios-admin.component";
/*import {HomeComponent} from "./home/home.component";
import {NewAlumnoComponent} from "./new-alumno/new-alumno.component";
import {AlumnoFormComponent} from "./alumno-form/alumno-form.component";
import {EditAlumnoComponent} from "./edit-alumno/edit-alumno.component";
import {AlumnoResolver} from "./shared/model/alumno.resolver";*/


export const routerConfig: Route[] = [
    {
        path: 'home',
        component: ListAlumnosComponent,
        pathMatch: 'full'

    },
    {
      path: 'maestra',
      redirectTo:'home/alumnos/maestra',      
      pathMatch: 'full'

  },
    {
      path: 'home/alumnos/:id/fichero-libros',
      component:  FicheroLibroComponent,
        children: [
          {
            path: '',
            component:  MenuComponent,
            outlet: 'lolo',
            resolve: {
              alumno: AlumnoResolver
            },
            pathMatch: 'full'
    
          }
        ],
    pathMatch: 'full'
    
    },
    {
      path: 'home/alumnos/:id/estado-prestamos',
      component:  EstadoActualPrestamosComponent,
        children: [
          {
            path: '',
            component:  MenuComponent,
            outlet: 'lolo',
            resolve: {
              alumno: AlumnoResolver
            },
            pathMatch: 'full'
    
          }
        ],
    pathMatch: 'full'
    
    },
    {
      path: 'home/alumnos/:id/comentarios',
      component:  ListComentariosAdminComponent,
        children: [
          {
            path: '',
            component:  MenuComponent,
            outlet: 'lolo',
            resolve: {
              alumno: AlumnoResolver
            },
            pathMatch: 'full'
    
          }
        ],
    pathMatch: 'full'
    
    },
    {
      path: 'home/alumnos/:id/admin-libros',
      component:  ListBookAdminComponent,
      children: [
        {
          path: '',
          component:  MenuComponent,
          outlet: 'lolo',
          resolve: {
            alumno: AlumnoResolver
          },
          pathMatch: 'full'
  
        }
      ],
  pathMatch: 'full'

  },
    {
      path: 'home/alumnos/:id/admin-alumnos',
      component:  ListAlumnosAdminComponent,
      children: [
        {
          path: '',
          component:  MenuComponent,
          outlet: 'lolo',
          resolve: {
            alumno: AlumnoResolver
          },
          pathMatch: 'full'
  
        }
      ],
  pathMatch: 'full'

  },
  {
    path: 'home/alumnos/:id/leidos',
    component:  ListLeidosComponent,
    children: [
      {
        path: '',
        component:  MenuComponent,
        outlet: 'lolo',
        resolve: {
          alumno: AlumnoResolver
        },
        pathMatch: 'full'

      }
    ],
    resolve: {
      alumno: AlumnoResolver
    },
    pathMatch: 'full'
  },
  {
    path: 'home/alumnos/:id/leidos/leidos',
    redirectTo: 'home/alumnos/:id/leidos'
  },
    {
      path: 'home/alumnos/:id/:book',
      component:  BookComponent,
      children: [
        {
          path: '',
          component:  MenuComponent,
          outlet: 'lolo',
          resolve: {
            alumno: AlumnoResolver,
            libro: BookResolver
          },
          pathMatch: 'full'

        }
      ],
      resolve: {
        alumno: AlumnoResolver
      },
      pathMatch: 'full'

    },
    {
        path: 'home/alumnos/:id/:book/comentarios',
        component:  BookComponent,
        children: [
            {
                path: '',
                component:  MenuComponent,
                outlet: 'lolo',
                resolve: {
                    alumno: AlumnoResolver,
                    libro: BookResolver
                },
                pathMatch: 'full'

            }
        ],
        resolve: {
            alumno: AlumnoResolver
        },
        pathMatch: 'full'

    },

    {
        path: 'home/alumnos/:id',
        component:  ListBookComponent,
        children: [
          {
            path: '',
            component:  MenuComponent,
            outlet: 'lolo',
            resolve: {
              alumno: AlumnoResolver
            },
            pathMatch: 'full'

          }
        ],
      pathMatch: 'full'

    },

  {
    path: 'rating',
    component:  ListBookComponent,
    children: [
      {
        path: '',
        component:  MenuComponent,
        outlet: 'lolo',
        resolve: {
          alumno: AlumnoResolver
        },
        pathMatch: 'full'

      }
    ],
    pathMatch: 'full'

  },
  {
      path: 'admin/libros',
      component:  ListBookAdminComponent,
      pathMatch: 'full'

  },
    {
      path: 'admin/alumnos',
      component:  ListAlumnosAdminComponent,
      pathMatch: 'full'

  },

  {
      path: '**',
      redirectTo: 'home'
  }
];





