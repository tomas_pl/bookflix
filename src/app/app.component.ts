import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {AlumnoService} from "./shared/model/alumno.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  alumnoId:string;
  nombre:string;
  alumno$;
  title = 'app works!';
  constructor( private router: Router, private route:ActivatedRoute) { }


  ngOnInit() {
      this.alumnoId = this.route.snapshot.params['id'];
   }
  register(){
    return this.alumnoId!="";
  }


}
