import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BookService } from 'app/shared/model/book.service';
import { Libro } from 'app/shared/model/libro';
import { AlumnoService } from 'app/shared/model/alumno.service';
import { Alumno } from 'app/shared/model/alumno';
declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-fichero-libro',
  templateUrl: './fichero-libro.component.html',
  styleUrls: ['./fichero-libro.component.css']
})
export class FicheroLibroComponent implements OnInit {
  booksSubscription: Subscription;
  alumnoSubscription: Subscription;
  ficheroLibrosSubscription: Subscription;
  private libros;
  private alumnos;
  private ficheroLibros;
  public fichero = [];
  constructor(private bookService: BookService, private alumnoService:AlumnoService) { }

  ngOnInit() {
    this.booksSubscription = this.bookService.findAllLibro().subscribe(response => {
      if(response){
        this.libros = response
      }
    })
    this.alumnoSubscription = this.alumnoService.findAllAlumno().subscribe(response => {
      if(response){
        this.alumnos = response
      }
    })

    this.ficheroLibrosSubscription = this.bookService.getFicheroLibros().subscribe(response => {
      if(response){
        this.fichero = []
        this.ficheroLibros = response
        this.ficheroLibros.forEach(ficha => {
        const titulo = this.libros.find(libro => libro.$key === ficha.$key).titulo;
        const imgLibro = this.libros.find(libro => libro.$key === ficha.$key).foto;
        const key = this.libros.find(libro => libro.$key === ficha.$key).$key;
        const libro = {imgLibro, titulo, key, alumnos:[]}
        Object.keys(ficha.list).forEach(bookKey => {
            const nombre = this.alumnos.find(l => l.$key === ficha.list[bookKey].idAlumno).nombre;
            const apellido = this.alumnos.find(l => l.$key === ficha.list[bookKey].idAlumno).apellido
            const img = this.alumnos.find(l => l.$key === ficha.list[bookKey].idAlumno).img
            libro.alumnos.push({nya: nombre+' '+apellido, img, fecha: ficha.list[bookKey].timestamp})           
        })
        this.fichero.push(libro)
        })
      }
    })
  }
  
  

  ngOnDestroy(){
    if(this.booksSubscription) this.booksSubscription.unsubscribe();
    if(this.alumnoSubscription) this.alumnoSubscription.unsubscribe();
    if(this.ficheroLibrosSubscription) this.ficheroLibrosSubscription.unsubscribe();
  }

}
