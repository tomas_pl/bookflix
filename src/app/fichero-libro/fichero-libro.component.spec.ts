import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheroLibroComponent } from './fichero-libro.component';

describe('FicheroLibroComponent', () => {
  let component: FicheroLibroComponent;
  let fixture: ComponentFixture<FicheroLibroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheroLibroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheroLibroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
