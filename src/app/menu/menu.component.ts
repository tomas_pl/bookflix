import {Component, OnInit, SimpleChanges} from '@angular/core';
import {AlumnoService} from "../shared/model/alumno.service";
import {Router, ActivatedRoute, ActivatedRouteSnapshot} from "@angular/router";
import {Alumno} from "../shared/model/alumno";
import {Libro} from "../shared/model/libro";
import { BookService } from 'app/shared/model/book.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  alumno:Alumno;
  libro:Boolean;
  leido:Boolean;
  esMaestra:Boolean;
  relPathFichero='';
  relPathPrestamos='';
  adminurl='';
  constructor(private route: ActivatedRoute, private router: Router,
			  private alumnoService: AlumnoService, private libroService: BookService) {
	route.data
	  .subscribe(
		data => {
		  if(!data['alumno'].nombre && data['alumno'].$key!=='maestra'){
			this.router.navigate(['/home'])
		  }else {
		  if(data['alumno'].$key==='maestra'){
			this.alumno = {nombre: 'Maestra', apellido: 'Maestra', img:'01', libro:null, $key:'maestra', id:'1'}
			this.esMaestra = true;
		  }else {
			this.esMaestra = false
			this.alumno = data['alumno'];
		  }
		}
		  this.libro = data['libro']?true:false;
		  this.leido = data['leido']?true:false;}
	  );

  }

  ngOnInit() {
	  console.log('this.libro', this.libro);
const splittedURL =this.router.routerState.snapshot.url.split('/')
this.adminurl = splittedURL[splittedURL.length - 1]



  }

  keyUp(event){
	this.libroService.searchBook(event.target.value)
  }


}
