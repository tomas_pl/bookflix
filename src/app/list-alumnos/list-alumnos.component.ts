import { Component, OnInit, ViewChild } from '@angular/core';
import {AlumnoService} from "../shared/model/alumno.service";
import {Observable} from "rxjs";
import {Alumno} from "../shared/model/alumno";
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-alumnos',
  templateUrl: './list-alumnos.component.html',
  styleUrls: ['./list-alumnos.component.css']
})
export class ListAlumnosComponent implements OnInit {

  alumno$: Observable<Alumno[]>;
  alumnoClicked: any;
  showErrorPassword = false;
  @ViewChild('logButton') password: any;
  @ViewChild('modalPassword') modalPassword: any;

  constructor( private alumnoService: AlumnoService, private router: Router) { }

  ngOnInit() {
    this.alumno$ = this.alumnoService.findAllAlumno();
  }

  login(){
    this.alumnoService.loginWithGoogle(this.alumnoClicked.$key, this.password.nativeElement.value).then((data) => {
      this.password.nativeElement.value = "";
      window.document.getElementsByClassName('fade')[1].remove();
      this.router.navigate(['/home/alumnos',this.alumnoClicked.$key ]);
    }, e => {
        this.showErrorPassword = true;
        this.password.nativeElement.value = "";
    })
  }

  updateData(alumno: Alumno){
    this.alumnoClicked = alumno;
    this.deletePassword();
  }
  hideError(){
    this.showErrorPassword = false;
  }
  deletePassword(){
    if(this.password){
      this.password.nativeElement.value = "";
    }
    this.showErrorPassword = false;
  }
}
