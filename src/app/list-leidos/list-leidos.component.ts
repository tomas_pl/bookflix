import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {BookService} from "../shared/model/book.service";
import {Leido} from "../shared/model/leido";
import {Alumno} from "../shared/model/alumno";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-list-leidos',
  templateUrl: './list-leidos.component.html',
  styleUrls: ['./list-leidos.component.css']
})
export class ListLeidosComponent implements OnInit {

  leidos$: Observable<Leido[]>;
  alumno:Alumno;


  constructor( private bookService: BookService,private route:ActivatedRoute,private router:Router) {
    route.data
      .subscribe(
        data => {this.alumno = data['alumno']}
      );
  }

  ngOnInit() {
    this.leidos$ = this.bookService.findLeidos(this.alumno);
  }

}
