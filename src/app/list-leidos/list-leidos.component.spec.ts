import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLeidosComponent } from './list-leidos.component';

describe('ListLeidosComponent', () => {
  let component: ListLeidosComponent;
  let fixture: ComponentFixture<ListLeidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListLeidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLeidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
