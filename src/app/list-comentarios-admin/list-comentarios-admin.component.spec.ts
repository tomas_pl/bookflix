import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListComentariosAdminComponent } from './list-comentarios-admin.component';

describe('ListComentariosAdminComponent', () => {
  let component: ListComentariosAdminComponent;
  let fixture: ComponentFixture<ListComentariosAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListComentariosAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComentariosAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
