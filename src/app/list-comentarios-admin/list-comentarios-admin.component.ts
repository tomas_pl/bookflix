
import { Component, OnInit } from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {BookService} from "../shared/model/book.service";
import {Leido} from "../shared/model/leido";
import {Alumno} from "../shared/model/alumno";
import {ActivatedRoute, Router} from "@angular/router";
import { Prestado } from 'app/shared/model/prestado';

@Component({
  selector: 'app-list-comentarios-admin',
  templateUrl: './list-comentarios-admin.component.html',
  styleUrls: ['./list-comentarios-admin.component.css']
})
export class ListComentariosAdminComponent implements OnInit {

  comentarios;
  comentarios$: Subscription
  alumno:Alumno;
  titulo;
  nya;
  idLibro;
  idAlumno;
  key;
  commentKey;
  libroKey;


  constructor( private bookService: BookService,private route:ActivatedRoute,private router:Router) {
    route.data
      .subscribe(
        data => {this.alumno = data['alumno']}
      );
  }

  ngOnInit() {
    this.comentarios$ = this.bookService.findComentariosAdmin().subscribe(response =>{
      if(response)
      this.comentarios =response.sort((a, b) => a['$key'] > b['$key'] ? 1 : a['$key'] === b['$key'] ? 0 : -1).reverse();
    });
  }

  devolvio(idLibro, idAlumno, key){
    this.bookService.devolucionEstado(idLibro, idAlumno, key)
  }

  devolvioModal(idLibro, idAlumno, key, titulo, nya){
    this.titulo = titulo;
    this.nya = nya;
    this.idLibro = idLibro;
    this.idAlumno = idAlumno;
    this.key = key;
  }
  eliminarModal(commentKey, libroKey){
    this.commentKey = commentKey
    this.libroKey = libroKey
  }
  eliminar(commentKey, libroKey){
    this.bookService.deleteComment(commentKey, libroKey)
  }
  ngOnDestroy(){
    if(this.comentarios$) this.comentarios$.unsubscribe();
  }

}
