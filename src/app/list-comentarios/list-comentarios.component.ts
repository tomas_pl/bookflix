import {Component, OnInit, Input, SimpleChanges, OnChanges} from '@angular/core';
import {BookService} from "../shared/model/book.service";
import {Observable} from "rxjs";
import {Comentario} from "../shared/model/comentario";

@Component({
  selector: 'app-list-comentarios',
  templateUrl: './list-comentarios.component.html',
  styleUrls: ['./list-comentarios.component.css']
})
export class ListComentariosComponent implements OnInit , OnChanges{
  comentarios$: Observable<Comentario[]>;
  @Input()
  initialValue: any;
  @Input()
  showEditComments: boolean;
  constructor(private bookService: BookService) {}



  ngOnInit() {

    this.comentarios$ = this.bookService.imprimirComentariosLibro(this.initialValue);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['initialValue']) {
      this.comentarios$ = this.bookService.imprimirComentariosLibro(changes['initialValue'].currentValue);
    }
  }

}
