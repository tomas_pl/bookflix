import {Component, OnInit, Input, ViewContainerRef, OnDestroy} from '@angular/core';
import {BookService} from "../shared/model/book.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Libro} from "../shared/model/libro";
import {Calificacion} from "../shared/model/calificacion";
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Alumno} from "../shared/model/alumno";
import { Overlay } from 'angular2-modal';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import {Comentario} from '../shared/model/comentario';
import { Subscription } from 'rxjs';
import { AlumnoService } from 'app/shared/model/alumno.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit, OnDestroy {
  form:FormGroup;
retirado: boolean;
video: boolean;
  @Input()
  initialValue:any;
  libro:Libro;
  alumnoId:string;
  alumno:Alumno;
  calificacion:Calificacion;
  showEditComments = false;
  esMaestra= false;
  linkYoutube:any;
  test:string;
  stringBook:string;
  subscriptionBook: Subscription
  commentSubscription: Subscription
  constructor(overlay: Overlay,
              vcRef: ViewContainerRef,
              private route:ActivatedRoute,
              private router:Router,
              private bookService:BookService,
              private alumnoService:AlumnoService,
              private fb:FormBuilder,
              private sanitizer: DomSanitizer,
              public modal: Modal) {
    route.data
      .subscribe(
        data => {this.alumno = data['alumno']}
      );
    this.form = this.fb.group({
      comentario: ['', [Validators.required]],
      alumno:[this.alumno.nombre],
      foto:[this.alumno.img ]
    });
    overlay.defaultViewContainer = vcRef;


  }

  ngOnInit() {
    window.scrollTo(0, 0)
    this.showEditComments = this.route.snapshot.routeConfig.path.indexOf('comentarios') > -1;

    this.subscriptionBook = this.route.params.switchMap(params => {

      const tituloUrl = params['book'];

      return this.bookService.findBookByUrl(tituloUrl);
    })
      .subscribe(libro => {
        this.libro = libro;
        this.stringBook = this.libro.$key;
        if(this.libro.linkYoutube){
        const auxUrl = this.libro.linkYoutube.split('?v=')[1]
        const parseLinkYoutube ="https://www.youtube-nocookie.com/embed/" + auxUrl.split('&')[0]+'?autoplay=1&controls=0&rel=0&enablejsapi=1'
        this.linkYoutube = this.sanitizer.bypassSecurityTrustResourceUrl(parseLinkYoutube);
        }
        this.route.params.switchMap(params => {

            this.alumnoId = params['id'];

            this.esMaestra = this.alumnoId==='maestra'
            this.retirado = this.libro.loTiene ===this.alumnoId
            this.alumnoService.findAlumnoById(this.alumnoId).subscribe(response => {
              this.alumno = response;
            })
            return this.bookService.findBookByCalif(this.stringBook, this.alumnoId);
        })
          .subscribe(result => this.test = result.$value);});
  }

  guardoCalificacion(calif,book,bookTitle,bookFoto){
      const alumnoId = this.route.snapshot.params['id'];
      this.bookService.savePromedioLibro(alumnoId,calif,book);
      this.bookService.actualizoPromedios(alumnoId,calif,book);

  }

  save(libro, titulo) {
    this.commentSubscription = this.bookService.createNewComment(this.form.value,libro, new Date().getTime(), titulo)
      .subscribe(
        () => {
          this.form = this.fb.group({
            comentario: ['', [Validators.required]],
            alumno:[this.alumno.nombre],
            foto:[this.alumno.img ]
        });        },
        err => alert(`error creating alumno ${err}`)
      );
    this.reset();

  }
  reset(){
      this.form = this.fb.group({
          comentario: [''],
          alumno:[this.alumno.nombre],
          foto:[this.alumno.img ]
      });
  }

  retirar(idLibro, idAlumno, nombreAlumno, apellidoAlumno, bookTitle, bookFoto){
    this.bookService.agregarAEstadoActual(idAlumno, nombreAlumno, apellidoAlumno, idLibro,bookTitle,bookFoto);
    this.bookService.retiroDeLibro(idAlumno,idLibro,bookTitle,bookFoto);
    this.bookService.agregarEnFichero(idLibro, idAlumno)
    this.bookService.retirar(idLibro, idAlumno)
    this.alumnoService.retirar(idLibro, idAlumno)
  }
  hideVideo(){
    this.video = false;
  }

  showVideo(){
    this.video = true;
  }
  ngOnDestroy(){
    if(this.subscriptionBook) this.subscriptionBook.unsubscribe();
    if(this.commentSubscription)this.commentSubscription.unsubscribe()
  }

}

